/******************************************************************************
Copyright (c) 2020 Valerio Orlandini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/

#include "m_pd.h"
#include "wtnet.h"

static t_class *neurosc_tilde_class;

typedef struct _neurosc_tilde
{
    t_object  x_obj;

    float f;

    float ramp;
    float freq;
    float inv_sample_rate;
    float wave[600];

    t_inlet *x_in_lat_1;
    t_inlet *x_in_lat_2;
    t_inlet *x_in_lat_3;
    t_inlet *x_in_lat_4;
    t_inlet *x_in_lat_5;
    t_inlet *x_in_lat_6;
    t_inlet *x_in_lat_7;
    t_inlet *x_in_lat_8;
    t_outlet *x_out;
} t_neurosc_tilde;

t_int *neurosc_tilde_perform(t_int *w)
{
    t_neurosc_tilde *x = (t_neurosc_tilde *)(w[1]);

    t_sample *freq = (t_sample *)(w[2]);

    t_sample *lat_1 = (t_sample *)(w[3]);
    t_sample *lat_2 = (t_sample *)(w[4]);
    t_sample *lat_3 = (t_sample *)(w[5]);
    t_sample *lat_4 = (t_sample *)(w[6]);
    t_sample *lat_5 = (t_sample *)(w[7]);
    t_sample *lat_6 = (t_sample *)(w[8]);
    t_sample *lat_7 = (t_sample *)(w[9]);
    t_sample *lat_8 = (t_sample *)(w[10]);

    t_sample *out = (t_sample *)(w[11]);

    unsigned int wave_sample[2] = {0, 1};
    float interp = 0.0;

    int n = (int)(w[12]);

    int i;
    
    for(i = 0; i < n; i++)
    {
      if (freq[i] > 0.0)
      {
        x->freq = freq[i];
      }

      x->ramp += x->freq * x->inv_sample_rate * 599.0;
      
      if (x->ramp > 599.0)
      {
        float latent[8] = {lat_1[i] * 10.0, lat_2[i] * 10.0, lat_3[i] * 10.0, lat_4[i] * 10.0, lat_5[i] * 10.0, lat_6[i] * 10.0, lat_7[i] * 10.0, lat_8[i] * 10.0};
        x->ramp = 0.0;
        generate_wave(latent, x->wave);
      }

      wave_sample[0] = (unsigned int)(floorf(x->ramp));
      wave_sample[1] = (wave_sample[0] + 1) % 600;
      interp = (float)(wave_sample[0] + 1) - x->ramp;
      
      out[i] = (x->wave[wave_sample[0]] * interp) + (x->wave[wave_sample[1]] * (1.0 - interp));
    }

    return (w + 13);
}

void neurosc_tilde_dsp(t_neurosc_tilde *x, t_signal **sp)
{
    dsp_add(neurosc_tilde_perform, 12, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[3]->s_vec, sp[4]->s_vec, sp[5]->s_vec, sp[6]->s_vec, sp[7]->s_vec, sp[8]->s_vec, sp[9]->s_vec, sp[0]->s_n);
    x->inv_sample_rate = 1.0 / sys_getsr();
    float latent[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    generate_wave(latent, x->wave);
}

void neurosc_tilde_free(t_neurosc_tilde *x)
{
    inlet_free(x->x_in_lat_1);
    inlet_free(x->x_in_lat_2);
    inlet_free(x->x_in_lat_3);
    inlet_free(x->x_in_lat_4);
    inlet_free(x->x_in_lat_5);
    inlet_free(x->x_in_lat_6);
    inlet_free(x->x_in_lat_7);
    inlet_free(x->x_in_lat_8);

    outlet_free(x->x_out);
}

void *neurosc_tilde_new(t_floatarg f)
{
    t_neurosc_tilde *x = (t_neurosc_tilde *)pd_new(neurosc_tilde_class);

    x->ramp = 0.0;
    x->freq = f;

    x->x_in_lat_1 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    x->x_in_lat_2 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    x->x_in_lat_3 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    x->x_in_lat_4 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    x->x_in_lat_5 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    x->x_in_lat_6 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    x->x_in_lat_7 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
    x->x_in_lat_8 = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);

    x->x_out = outlet_new(&x->x_obj, &s_signal);

    return (void *)x;
}

void neurosc_tilde_setup(void)
{
    neurosc_tilde_class = class_new(gensym("neurosc~"), (t_newmethod)neurosc_tilde_new,
                                    (t_method)neurosc_tilde_free, sizeof(t_neurosc_tilde), CLASS_DEFAULT, A_DEFFLOAT, 0);

    class_addmethod(neurosc_tilde_class, (t_method)neurosc_tilde_dsp, gensym("dsp"), 0);
    CLASS_MAINSIGNALIN(neurosc_tilde_class, t_neurosc_tilde, f);
}
