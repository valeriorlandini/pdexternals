/******************************************************************************
Copyright (c) 2020 Valerio Orlandini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/

#include <math.h>
#include "m_pd.h"

inline float fclampf(float *in, float low_bound, float hi_bound)
{
    float out = *in;

    if (out < low_bound)
    {
        out = low_bound;
    }
    if (out > hi_bound)
    {
        out = hi_bound;
    }

    return out;
}

inline float chebyshev_polynomial(float *input, float *order)
{
    unsigned int lo_ord = floorf(*order);
    unsigned int hi_ord = ceilf(*order);
    float blend = *order - lo_ord;

    float out_lo = 0.0;
    float out_hi = 0.0;

    switch (lo_ord)
    {
    case 0:
        out_lo = 1.0;
        break;
    case 1:
        out_lo = *input;
        break;
    case 2:
        out_lo = 2.0 * powf(*input, 2.0) - 1.0;
        break;
    case 3:
        out_lo = 4.0 * powf(*input, 3.0) - 3.0 * *input;
        break;
    case 4:
        out_lo = 8.0 * powf(*input, 4.0) - 8.0 * powf(*input, 2.0) + 1.0;
        break;
    case 5:
        out_lo = 16.0 * powf(*input, 5.0) - 20.0 * powf(*input, 3.0) + 5.0 * *input;
        break;
    case 6:
        out_lo = 32.0 * powf(*input, 6.0) - 48.0 * powf(*input, 4.0) + 18.0 * powf(*input, 2.0) - 1.0;
        break;
    case 7:
        out_lo = 64.0 * powf(*input, 7.0) - 112.0 * powf(*input, 5.0) + 56.0 * powf(*input, 3.0) - 7.0 * *input;
        break;
    case 8:
        out_lo = 128.0 * powf(*input, 8.0) - 256.0 * powf(*input, 6.0) + 160.0 * powf(*input, 4.0) - 32.0 * powf(*input, 2.0) + 1.0;
        break;
    case 9:
        out_lo = 256.0 * powf(*input, 9.0) - 576.0 * powf(*input, 7.0) + 432.0 * powf(*input, 5.0) - 120.0 * powf(*input, 3.0) + 9.0 * *input;
        break;
    case 10:
        out_lo = 512.0 * powf(*input, 10.0) - 1280.0 * powf(*input, 8.0) + 1120.0 * powf(*input, 6.0) - 400.0 * powf(*input, 4.0) + 50.0 * powf(*input, 2.0) - 1.0;
        break;
    default:
        out_lo = 0.0;
    }

    if (blend > 0.0)
    {
        switch (hi_ord)
        {
        case 0:
            out_hi = 1.0;
            break;
        case 1:
            out_hi = *input;
            break;
        case 2:
            out_hi = 2.0 * powf(*input, 2.0) - 1.0;
            break;
        case 3:
            out_hi = 4.0 * powf(*input, 3.0) - 3.0 * *input;
            break;
        case 4:
            out_hi = 8.0 * powf(*input, 4.0) - 8.0 * powf(*input, 2.0) + 1.0;
            break;
        case 5:
            out_hi = 16.0 * powf(*input, 5.0) - 20.0 * powf(*input, 3.0) + 5.0 * *input;
            break;
        case 6:
            out_hi = 32.0 * powf(*input, 6.0) - 48.0 * powf(*input, 4.0) + 18.0 * powf(*input, 2.0) - 1.0;
            break;
        case 7:
            out_hi = 64.0 * powf(*input, 7.0) - 112.0 * powf(*input, 5.0) + 56.0 * powf(*input, 3.0) - 7.0 * *input;
            break;
        case 8:
            out_hi = 128.0 * powf(*input, 8.0) - 256.0 * powf(*input, 6.0) + 160.0 * powf(*input, 4.0) - 32.0 * powf(*input, 2.0) + 1.0;
            break;
        case 9:
            out_hi = 256.0 * powf(*input, 9.0) - 576.0 * powf(*input, 7.0) + 432.0 * powf(*input, 5.0) - 120.0 * powf(*input, 3.0) + 9.0 * *input;
            break;
        case 10:
            out_hi = 512.0 * powf(*input, 10.0) - 1280.0 * powf(*input, 8.0) + 1120.0 * powf(*input, 6.0) - 400.0 * powf(*input, 4.0) + 50.0 * powf(*input, 2.0) - 1.0;
            break;
        default:
            out_hi = 0.0;
        }
    }

    return (out_lo * (1.0 - blend)) + (out_hi * blend);
}

static t_class *chebypoly_tilde_class;

typedef struct _chebypoly_tilde
{
    t_object  x_obj;

    float f;

    float order;

    t_inlet *x_order;
    t_outlet *x_out;
} t_chebypoly_tilde;

t_int *chebypoly_tilde_perform(t_int *w)
{
    t_chebypoly_tilde *x = (t_chebypoly_tilde *)(w[1]);

    t_sample *in = (t_sample *)(w[2]);
    t_sample *order = (t_sample *)(w[3]);

    t_sample *out = (t_sample *)(w[4]);

    int n = (int)(w[5]);

    int i;

    float ord = 0.0;

    for(i = 0; i < n; i++)
    {
        if (order[i] > 0.0)
        {
            x->order = order[i];
        }
        
        ord = fclampf(&(x->order), 0.0, 10.0);
        out[i] = chebyshev_polynomial(&in[i], &ord);
    }

    return (w + 6);
}

void chebypoly_tilde_dsp(t_chebypoly_tilde *x, t_signal **sp)
{
    dsp_add(chebypoly_tilde_perform, 5, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n);
}

void chebypoly_tilde_free(t_chebypoly_tilde *x)
{
    inlet_free(x->x_order);

    outlet_free(x->x_out);
}

void *chebypoly_tilde_new(t_floatarg f)
{
    t_chebypoly_tilde *x = (t_chebypoly_tilde *)pd_new(chebypoly_tilde_class);

    x->order = f;

    x->x_order = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);

    x->x_out = outlet_new(&x->x_obj, &s_signal);

    return (void *)x;
}

void chebypoly_tilde_setup(void)
{
    chebypoly_tilde_class = class_new(gensym("chebypoly~"), (t_newmethod)chebypoly_tilde_new,
                                      (t_method)chebypoly_tilde_free, sizeof(t_chebypoly_tilde), CLASS_DEFAULT, A_DEFFLOAT, 0);

    class_addmethod(chebypoly_tilde_class, (t_method)chebypoly_tilde_dsp, gensym("dsp"), 0);
    CLASS_MAINSIGNALIN(chebypoly_tilde_class, t_chebypoly_tilde, f);
}
