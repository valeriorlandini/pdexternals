/******************************************************************************
Copyright (c) 2020 Valerio Orlandini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/

#ifndef VA_OSC_H_
#define VA_OSC_H_

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#ifndef M_PI
#define M_PI 3.14159265358979324
#endif

typedef struct
{
    float frequency;
    float sample_rate;
    float inv_sample_rate;
    float half_sample_rate;
    float step;
    float drift;

    float sine_out;

    _Bool tri_up;
    float tri_out;

    float saw_out;

    float inv_saw_out;

    float pulse_width;
    float pulse_out;

    float noise_out;
} va_osc;

_Bool va_osc_init(va_osc *oscillator, float sample_rate, float frequency);
_Bool va_osc_set_sample_rate(va_osc *oscillator, float sample_rate);
_Bool va_osc_set_frequency(va_osc *oscillator, float frequency);
_Bool va_osc_set_pulse_width(va_osc *oscillator, float pulse_width);
_Bool va_osc_set_drift(va_osc *oscillator, float drift);
void va_osc_reset(va_osc *oscillator);
void va_osc_run(va_osc *oscillator);

#endif // VA_OSC_H_
