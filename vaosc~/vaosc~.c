/******************************************************************************
Copyright (c) 2020 Valerio Orlandini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/

#include "m_pd.h"
#include "va_osc.h"

static t_class *vaosc_tilde_class;

typedef struct _vaosc_tilde {
  t_object  x_obj;
  va_osc oscillator;

  float f;

  t_inlet *x_in_pw;
  t_outlet *x_sine_out;
  t_outlet *x_saw_out;
  t_outlet *x_tri_out;
  t_outlet *x_pulse_out;
  t_outlet *x_noise_out;
} t_vaosc_tilde;

t_int *vaosc_tilde_perform(t_int *w)
{
  t_vaosc_tilde *x = (t_vaosc_tilde *)(w[1]);

  t_sample *freq = (t_sample *)(w[2]);
  t_sample *pw = (t_sample *)(w[3]);
 
  t_sample *out_sine = (t_sample *)(w[4]);
  t_sample *out_saw = (t_sample *)(w[5]);
  t_sample *out_tri = (t_sample *)(w[6]);
  t_sample *out_pulse = (t_sample *)(w[7]);
  t_sample *out_noise = (t_sample *)(w[8]);

  int n = (int)(w[9]);
  
  int i;

  for(i = 0; i < n; i++)
  {
    if (freq[i] != x->oscillator.frequency && freq[i] != 0.0)
    {
      va_osc_set_frequency(&(x->oscillator), freq[i]);
    }

    if (pw[i] != x->oscillator.pulse_width && pw[i] != 0.0)
    {
      va_osc_set_pulse_width(&(x->oscillator), pw[i]);
    }
    
    va_osc_run(&(x->oscillator));

    out_sine[i] = x->oscillator.sine_out;
    out_saw[i] = x->oscillator.saw_out;
    out_tri[i] = x->oscillator.tri_out;
    out_pulse[i] = x->oscillator.pulse_out;
    out_noise[i] = x->oscillator.noise_out;
  }

  return (w + 10);
}

void vaosc_tilde_dsp(t_vaosc_tilde *x, t_signal **sp)
{
  dsp_add(vaosc_tilde_perform, 9, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[3]->s_vec, sp[4]->s_vec, sp[5]->s_vec, sp[6]->s_vec, sp[0]->s_n);
  va_osc_set_sample_rate(&(x->oscillator), sys_getsr());
  va_osc_reset(&(x->oscillator));
  va_osc_set_drift(&(x->oscillator), 0.001);
}

void vaosc_tilde_bang(t_vaosc_tilde *x)
{
  va_osc_reset(&(x->oscillator));
}

void vaosc_tilde_free(t_vaosc_tilde *x)
{
  inlet_free(x->x_in_pw);

  outlet_free(x->x_sine_out);
  outlet_free(x->x_saw_out);
  outlet_free(x->x_tri_out);
  outlet_free(x->x_pulse_out);
  outlet_free(x->x_noise_out);
}

void *vaosc_tilde_new(t_floatarg f)
{
  t_vaosc_tilde *x = (t_vaosc_tilde *)pd_new(vaosc_tilde_class);

  float freq = f;

  va_osc_init(&(x->oscillator), sys_getsr(), freq);
  
  x->x_in_pw = inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);

  x->x_sine_out = outlet_new(&x->x_obj, &s_signal);
  x->x_saw_out = outlet_new(&x->x_obj, &s_signal);
  x->x_tri_out = outlet_new(&x->x_obj, &s_signal);
  x->x_pulse_out = outlet_new(&x->x_obj, &s_signal);
  x->x_noise_out = outlet_new(&x->x_obj, &s_signal);

  return (void *)x;
}

void vaosc_tilde_setup(void) {
  vaosc_tilde_class = class_new(gensym("vaosc~"), (t_newmethod)vaosc_tilde_new,
  (t_method)vaosc_tilde_free, sizeof(t_vaosc_tilde), CLASS_DEFAULT, A_DEFFLOAT, 0);

  class_addmethod(vaosc_tilde_class, (t_method)vaosc_tilde_dsp, gensym("dsp"), 0);
  CLASS_MAINSIGNALIN(vaosc_tilde_class, t_vaosc_tilde, f);
  class_addbang(vaosc_tilde_class, vaosc_tilde_bang);
}
