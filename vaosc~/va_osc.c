/******************************************************************************
Copyright (c) 2020 Valerio Orlandini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/

#include "va_osc.h"

_Bool va_osc_init(va_osc *oscillator, float sample_rate, float frequency)
{
    _Bool success = 1;

    oscillator->sample_rate = 44100.0;
    oscillator->frequency = 440.0;
    oscillator->pulse_width = 0.5;
    oscillator->drift = 0.01;

    if (!va_osc_set_sample_rate(oscillator, sample_rate))
    {
#ifndef NO_ERR_MESSAGES
        fprintf(stderr, "Sample rate set to 44100 Hz\n");
#endif
        success = 0;

        va_osc_set_sample_rate(oscillator, 44100.0);
    }


    if (!va_osc_set_frequency(oscillator, frequency))
    {
#ifndef NO_ERR_MESSAGES
        fprintf(stderr, "Frequency set to 440 Hz\n");
#endif
        success = 0;

        va_osc_set_frequency(oscillator, 440.0);
    }

    srand(time(NULL));

    return success;
}

_Bool va_osc_set_sample_rate(va_osc *oscillator, float sample_rate)
{
    if (sample_rate > 0.0)
    {
        oscillator->sample_rate = sample_rate;
        oscillator->inv_sample_rate = 1.0 / sample_rate;
        oscillator->half_sample_rate = sample_rate * 0.5;

        return va_osc_set_frequency(oscillator, oscillator->frequency);
    }

#ifndef NO_ERR_MESSAGES
    fprintf(stderr, "Sample rate must be higher than zero\n");
#endif

    return 0;
}

_Bool va_osc_set_frequency(va_osc *oscillator, float frequency)
{
    if (fabs(frequency) <= oscillator->half_sample_rate)
    {
        oscillator->step = 2.0 * (frequency * oscillator->inv_sample_rate);
        oscillator->frequency = frequency;

        return 1;
    }

#ifndef NO_ERR_MESSAGES
    fprintf(stderr, "Frequency must be lower than %f Hz\n", oscillator->half_sample_rate);
#endif

    return 0;
}

_Bool va_osc_set_pulse_width(va_osc *oscillator, float pulse_width)
{
    if (pulse_width >= 0.0 && pulse_width <= 1.0)
    {
        oscillator->pulse_width = pulse_width;

        return 1;
    }

#ifndef NO_ERR_MESSAGES
    fprintf(stderr, "Pulse width value must be inside [0.0, 1.0] range\n");
#endif

    return 0;
}

_Bool va_osc_set_drift(va_osc *oscillator, float drift)
{
    oscillator->drift = drift;

    return 1;
}

void va_osc_reset(va_osc *oscillator)
{
    oscillator->sine_out = 0.0;
    oscillator->tri_out = 0.0;
    oscillator->tri_up = 1;
    oscillator->saw_out = 0.0;
    oscillator->pulse_out = 0.0;
    oscillator->noise_out = 0.0;
}

void va_osc_run(va_osc *oscillator)
{
    oscillator->noise_out = (((float)rand() / (float)RAND_MAX) * 2.0) - 1.0;
 
    float drift = oscillator->noise_out * oscillator->drift;
    float step = oscillator->step + drift;

    oscillator->saw_out += step;
    while (oscillator->saw_out > 1.0)
    {
        oscillator->saw_out -= 2.0;
    }

    // In case of negative frequency (that causes an inverted saw)
    while (oscillator->saw_out < -1.0)
    {
        oscillator->saw_out += 2.0;
    }

    oscillator->tri_out += oscillator->tri_up ? (step * 2.0) : (step * -2.0);
    if (oscillator->tri_out > 1.0)
    {
        oscillator->tri_up = 0;
        oscillator->tri_out = 1.0;
    }
    if (oscillator->tri_out < -1.0)
    {
        oscillator->tri_up = 1;
        oscillator->tri_out = -1.0;
    }

    oscillator->pulse_out = ((oscillator->saw_out + 1.0) * 0.5) < oscillator->pulse_width ?  1.0 : -1.0;

    oscillator->sine_out = sin(oscillator->saw_out * M_PI);
}
