# pdexternals

Collection of miscellaneous externals for Pure Data.

## chebypoly~
Waveshaper based on Chebyshev polynomials of first kind (https://en.wikipedia.org/wiki/Chebyshev_polynomials) up to 10th order.
### Inlets
1 (signal/float): audio input.\
2 (signal/float): polynomial order in (0.0, 10.0] range. Values outside this range are clamped inside it.
### Arguments
order (float): polynomial order in [0.0, 10.0] range. Values outside this range are clamped inside it.
### Outlets
1 (signal): audio output.

## neurosc~
Oscillator that lets you explore the latent space of an autoencoder (https://en.wikipedia.org/wiki/Autoencoder), a particular deep neural network trained to reconstruct the inputs through a compressed representation. The autoencoder was trained on over 15000 waveforms and its weights was exported and implemented in this external. By changing the eight parameters of the latent space you can obtain evolving complex and unusual waveforms.
### Inlets
1 (signal/float): oscillator frequency.\
2-9 (signal/float): the eight parameters of the latent space. They are optimized to receive an input between 0.0 and 1.0, but sending higher or lower values is fine.
### Arguments
freq (float): oscillator frequency.
### Outlets
1 (signal): oscillator output.

## vaosc~
Virtual analog oscillator producing the most common waveshapes found in classic synthesizers. The waves are generated through math and not by reading a wavetable.
### Inlets
1 (signal/float): oscillator frequency.\
2 (signal/float): pulse width for the pulse oscillator.
### Arguments
freq (float): oscillator frequency.
### Outlets
1-5 (signal): oscillator outputs (sine, saw, triangle, pulse, white noise).
